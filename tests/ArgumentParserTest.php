<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

use Apine\DistRoute\Argument;
use Apine\DistRoute\ArgumentParser;
use PHPUnit\Framework\TestCase;

class ArgumentParserTest extends TestCase
{
    public function testParse(): void
    {
        $parser = new ArgumentParser('/test/{input:([0-9]+)}/{?option}');
    
        $this->assertEquals(
            [
                new Argument('input', '([0-9]+)', false),
                new Argument('option', '([^\/]+?)', true)
            ],
            $parser->parse()
        );
    }
    
    public function testParseWhenNoArguments(): void
    {
        $parser = new ArgumentParser('/test');
        
        $this->assertEquals(
            [],
            $parser->parse()
        );
    }
    
    public function testParseIfInvalidArgumentFormat(): void
    {
        $parser = new ArgumentParser('/test/{input=([0-9]+)}');
    
        $this->assertEquals(
            [],
            $parser->parse()
        );
    }
    
    public function testParseWhenArgumentNotNamed(): void
    {
        $parser = new ArgumentParser('/test/{([A-Za-z]+)}');
    
        $this->assertEquals(
            [],
            $parser->parse()
        );
    }
}
