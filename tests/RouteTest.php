<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpParamsInspection */
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpDocMissingThrowsInspection */
/** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

use Apine\DistRoute\Route;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RouteTest extends TestCase
{
    public function testMatch(): void
    {
        $resolver = $this->createMock(\Apine\Resolver\DependencyResolver::class);
        $requestWithout = $this->mockRequestWithoutOptional();
        $requestWith = $this->mockRequestWithOptional();
        
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{input:([0-9]+)}/{?option}',
            function () {}
        );
        
        $this->assertTrue($route->match($requestWithout));
        $this->assertTrue($route->match($requestWith));
    }
    
    public function testMatchShouldAcceptAnyMethodsWhenAllowedMethodsArrayIsEmpty(): void
    {
        $resolver = $this->createMock(\Apine\Resolver\DependencyResolver::class);
        $requestPOST = $this->mockRequestMethodPost();
        $requestFUNKY = $this->mockRequestMethodFunky();
    
        $route = new Route(
            $resolver,
            [],
            '/test/{input:([0-9]+)}/{?option}',
            function () {}
        );
    
        $this->assertTrue($route->match($requestPOST));
        $this->assertTrue($route->match($requestFUNKY));
    }
    
    public function testMatchShouldReturnFalseWhenPathNotMatch(): void
    {
        $resolver = $this->createMock(\Apine\Resolver\DependencyResolver::class);
        $requestWithout = $this->mockRequestWithoutOptional();
    
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{input:([0-9]+)}/{option}',
            function () {}
        );
    
        $this->assertFalse($route->match($requestWithout));
    }
    
    public function testMatchShouldReturnFalseWhenMethodNotAllowed(): void
    {
        $resolver = $this->createMock(\Apine\Resolver\DependencyResolver::class);
        $requestWithout = $this->mockRequestWithoutOptional(); // Has method GET
    
        $route = new Route(
            $resolver,
            ['POST'],
            '/test/{input:([0-9]+)}/{?option}',
            function () {}
        );
    
        $this->assertFalse($route->match($requestWithout));
    }
    
    public function testInvoke(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/param/15');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
        
        $resolver = new \Apine\Resolver\DependencyResolver($container);
    
        $response = $this->getMockBuilder(ResponseInterface::class)
            ->setMethods(['getBody'])
            ->getMockForAbstractClass();
    
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            function (string $first, int $second = 404) use ($response) {
                $result = $first.$second;
                $response->method('getBody')->willReturn($result);
                return $response;
            }
        );
        
        $response = $route->invoke($request);
        
        $this->assertEquals('param15', $response->getBody());
    }
    
    public function testInvokeWithDependencyInjection(): void
    {
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/param');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
    
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['get', 'has'])
            ->getMockForAbstractClass();
    
        $container->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'first',
                    'second'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false,
                    false
                ]
            );
    
        $container->method('get')
            ->with($this->equalTo('response'))
            ->willReturnCallback(function () {
                $response = $this->getMockBuilder(ResponseInterface::class)
                    ->setMethods(['getBody'])
                    ->getMockForAbstractClass();
                $response->method('getBody')->willReturn('foo');
                return $response;
            });
    
        $resolver = new \Apine\Resolver\DependencyResolver($container);
    
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            function (ResponseInterface $response, string $first, int $second = 404) {
                $result = $first.$second;
                return $response;
            }
        );
    
        $response = $route->invoke($request);
    
        $this->assertEquals('foo', $response->getBody());
    }
    
    public function testInvokeControllerClass(): void
    {
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
        
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
        
        $uri->method('getPath')->willReturn('/test/param/15');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
    
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['get', 'has'])
            ->getMockForAbstractClass();
    
        $container->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'first',
                    'second'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false,
                    false
                ]
            );
    
        $container->method('get')
            ->with($this->equalTo('response'))
            ->willReturnCallback(function () {
                $response = $this->getMockBuilder(ResponseInterface::class)
                    ->setMethods(['getBody'])
                    ->getMockForAbstractClass();
                $response->method('getBody')->willReturn('foo');
                return $response;
            });
    
        $resolver = new \Apine\Resolver\DependencyResolver($container);
        
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            TestController::class . '@test'
        );
        
        $response = $route->invoke($request);
    
        $this->assertEquals('foo', $response->getBody());
    }
    
    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /(.+?) must return an instance of (.+?)/
     */
    public function testInvokeDoesNotReturnResponse(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/param/15');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
        $resolver = $this->createMock(\Apine\Resolver\DependencyResolver::class);
    
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            function (string $first, int $second = 404) {
                $result = $first.$second;
            }
        );
    
        $response = $route->invoke($request);
    }
    
    public function testProcess(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/param/15');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
    
        $resolver = new \Apine\Resolver\DependencyResolver($container);
        $handler = $this->getMockForAbstractClass(RequestHandlerInterface::class);
    
        $response = $this->getMockBuilder(ResponseInterface::class)
            ->setMethods(['getBody'])
            ->getMockForAbstractClass();
        $response->method('getBody')->willReturn('foo');
    
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            function() use ($response) {
                return $response;
            }
        );
    
        $response = $route->process($request, $handler);
    
        $this->assertEquals('foo', $response->getBody());
    }
    
    public function testProcessWhenCallableIsMiddleware(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
        
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
        
        $uri->method('getPath')->willReturn('/test/param/15');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
        
        $resolver = new \Apine\Resolver\DependencyResolver($container);
        
        $response = $this->getMockBuilder(ResponseInterface::class)
            ->setMethods(['getBody'])
            ->getMockForAbstractClass();
        $response->method('getBody')->willReturn('foo');
        
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function () use ($response) {
            return $response;
        });
        
        $handler = $this->getMockForAbstractClass(RequestHandlerInterface::class);
        
        $route = new Route(
            $resolver,
            ['GET'],
            '/test/{first}/{?second}',
            $middleware
        );
        
        $response = $route->process($request, $handler);
        
        $this->assertEquals('foo', $response->getBody());
    }
    
    private function mockRequestWithoutOptional(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('GET');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
        
        return $mockRequest;
    }
    
    private function mockRequestWithOptional(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('GET');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789/optional');
            return $mockUri;
        });
        
        return $mockRequest;
    }
    
    private function mockRequestMethodPost(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('POST');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
        
        return $mockRequest;
    }
    
    private function mockRequestMethodFunky(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('FUNKY');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
        
        return $mockRequest;
    }
}

class TestController {
    public function test(ResponseInterface $response, string $first, int $second = 2): ResponseInterface {
        return $response;
    }
}