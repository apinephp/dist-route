<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

use Apine\DistRoute\Argument;
use Apine\DistRoute\ArgumentParserInterface;
use Apine\DistRoute\RegexBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RegexBuilderTest extends TestCase
{
    static private $regexValidationPattern = '/^((?:(?:[^?+*{}()[\]\\|]+|\\.|\[(?:\^?\\.|\^[^\\]|[^\\^])(?:[^\]\\]+|\\.)*\]|\((?:\?[:=!]|\?<[=!]|\?>)?(?1)??\)|\(\?(?:R|[+-]?\d+)\))(?:(?:[?+*]|\{\d+(?:,\d*)?\})[?+]?)?|\|)*)$/';
    
    static private $argumentDefinitionRegex = '/\{(\??)(\w+?)(:(\(.+?\)))?\}/';
    
    public function testBuild(): void
    {
        $pattern = '/test/{input:([0-9]+)}/{?option}';
        $parser = $this->mockParser();
        $builder = new RegexBuilder($parser, $pattern);
        
        $this->assertIsValidRegex($builder->build());
    }
    
    public function testBuildWhenPatternDoesNotMatchParserShouldStillContainArgumentDefinition(): void
    {
        $pattern = '/test/{name:([A-Za-z]+)}';
        $parser = $this->mockParser();
        $builder = new RegexBuilder($parser, $pattern);
    
        $this->assertStringMatchesRegex(self::$argumentDefinitionRegex, $builder->build());
    }
    
    private function mockParser(): ArgumentParserInterface
    {
        /** @var ArgumentParserInterface | MockObject $mockParser */
        $mockParser = $this->getMockBuilder(ArgumentParserInterface::class)
            ->setMethods(['parse'])
            ->getMockForAbstractClass();
        $mockParser->method('parse')->willReturn([
            $this->getMockBuilder(Argument::class)->setConstructorArgs([
                'input',
                '([0-9]+)',
                false
            ])->getMock(),
            $this->getMockBuilder(Argument::class)->setConstructorArgs([
                'option',
                '([^\/]+?)',
                true
            ])->getMock()
        ]);
        
        return $mockParser;
    }
    
    private function assertIsValidRegex(string $regex): void
    {
        $match = (@preg_match($regex, self::$regexValidationPattern) !== false);
        static::assertThat($match, static::isTrue());
    }
    
    private function assertStringMatchesRegex(string $pattern, string $subject): void
    {
        $matches = [];
        @preg_match($pattern, $subject, $matches);
        static::assertNotEmpty($matches);
    }
}
