<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpParamsInspection */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpMethodParametersCountMismatchInspection */

declare(strict_types=1);

use Apine\DistRoute\Router;
use Apine\DistRoute\RouterInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;

class RouterTest extends TestCase
{
    public function testGetSetBasePattern(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $router->setBasePattern('/test');
    
        $this->assertEquals('/test', $router->getBasePattern());
    }
    
    public function testUse(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        
        $request = $this->getMockForAbstractClass(ServerRequestInterface::class);
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        $response->method('getBody')->willReturn('foo');
        
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function () use ($response) {
            return $response;
        });
        
        $result = $router->use($middleware);
        
        $this->assertEquals('foo', $result->process($request, $router)->getBody());
    }
    
    public function testMap(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->map(['GET', 'DELETE'], '/test/{number}', function(){});
    
        $requestDelete = $this->mockRequest('DELETE');
        $this->assertTrue($route->match($requestDelete));
    
        $requestGet = $this->mockRequest('GET');
        $this->assertTrue($route->match($requestGet));
    }
    
    public function testGet(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->get('/test/{number}', function(){});
    
        $requestGet = $this->mockRequest('GET');
        $this->assertTrue($route->match($requestGet));
    }
    
    public function testPost(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->post('/test/{number}', function(){});
    
        $requestPost = $this->mockRequest('POST');
        $this->assertTrue($route->match($requestPost));
    }
    
    public function testDelete(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->delete('/test/{number}', function(){});
    
        $requestDelete = $this->mockRequest('DELETE');
        $this->assertTrue($route->match($requestDelete));
    }
    
    public function testPut(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->put('/test/{number}', function(){});
    
        $requestPut = $this->mockRequest('PUT');
        $this->assertTrue($route->match($requestPut));
    }
    
    public function testTrace(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->trace('/test/{number}', function(){});
        
        $requestTrace = $this->mockRequest('TRACE');
        $this->assertTrue($route->match($requestTrace));
    }
    
    public function testOptions(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->options('/test/{number}', function(){});
    
        $requestOptions = $this->mockRequest('OPTIONS');
        $this->assertTrue($route->match($requestOptions));
    }
    
    public function testHead(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->head('/test/{number}', function(){});
    
        $requestHead = $this->mockRequest('HEAD');
        $this->assertTrue($route->match($requestHead));
    }
    
    public function testMapWithCustom(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->map(['PATCH'], '/test/{number}', function(){});
    
        $requestPatch = $this->mockRequest('PATCH');
        $this->assertTrue($route->match($requestPatch));
    }
    
    public function testAny(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $route = $router->any('/test/{number}', function(){});
    
        $requestPatch = $this->mockRequest('PATCH');
        $this->assertTrue($route->match($requestPatch));
    
        $requestPut = $this->mockRequest('PUT');
        $this->assertTrue($route->match($requestPut));
    
        $requestDelete = $this->mockRequest('DELETE');
        $this->assertTrue($route->match($requestDelete));
    
        $requestGet = $this->mockRequest('GET');
        $this->assertTrue($route->match($requestGet));
    }
    
    public function testGroup(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $parent = $this;
        
        $router->group('/test', function ($mapper) use ($parent) {
            $parent->assertInstanceOf(RouterInterface::class, $mapper);
            $route = $mapper->any('/{number}', function(){});
    
            $requestPatch = $parent->mockRequest('PATCH');
            $parent->assertTrue($route->match($requestPatch));
        });
    }
    
    public function testHandleWithMiddleware(): void
    {
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
    
        $request = $this->getMockForAbstractClass(ServerRequestInterface::class);
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        $response->method('getBody')->willReturn('foo');
    
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function () use ($response) {
            return $response;
        });
    
        $router->use($middleware);
        $result = $router->handle($request);
        
        $this->assertEquals('foo', $result->getBody());
    }
    
    public function testHandleWithMiddlewareAndRoute(): void
    {
        $parent = $this;
        $diResponse = $this->getMockForAbstractClass(ResponseInterface::class);
        $diResponse->method('withBody')->willReturnCallback(function ($body) use ($diResponse) {
            $stream = $this->getMockForAbstractClass(StreamInterface::class);
            $stream->method('__toString')->willReturn((string)$body);
            $diResponse->method('getBody')->willReturn($stream);
            return $diResponse;
        });
        
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['get', 'has'])
            ->getMockForAbstractClass();
        
        $container->method('has')->with($this->equalTo('response'))->willReturn(true);
        $container->method('get')->with($this->equalTo('response'))->willReturn($diResponse);
        
        $router = new Router($container);
    
        $request = $this->mockRequest('GET');
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        $response->method('withBody')->willReturnCallback(function ($body) use ($response, $parent) {
            $stream = $parent->getMockForAbstractClass(StreamInterface::class);
            $stream->method('__toString')->willReturn((string)$body);
            $response->method('getBody')->willReturn($stream);
            return $response;
        });
    
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function ($request, $handler) use ($response, $parent) {
            $body = 'before-middleware';
            
            $childResponse = $handler->handle($request);
            $body = $body . '/' . $childResponse->getBody();
    
            $streamAfter = $parent->getMockForAbstractClass(StreamInterface::class);
            $streamAfter->method('__toString')->willReturn($body . '/after-middleware');
            return $response->withBody($streamAfter);
        });
        
        $routedMiddleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $routedMiddleware->method('process')->willReturnCallback(function ($request, $handler) use ($parent) {
            $response = $parent->getMockForAbstractClass(ResponseInterface::class);
            $response->method('withBody')->willReturnCallback(function ($body) use ($response, $parent) {
                $stream = $parent->getMockForAbstractClass(StreamInterface::class);
                $stream->method('__toString')->willReturn((string)$body);
                $response->method('getBody')->willReturn($stream);
                return $response;
            });
            
            
            $childResponse = $handler->handle($request);
            $body = 'routed-middleware/' . $childResponse->getBody();
            
            $stream = $parent->getMockForAbstractClass(StreamInterface::class);
            $stream->method('__toString')->willReturn($body);
            return $response->withBody($stream);
        });
    
        $router->use($middleware);
        $router->any('/test/{number}', $routedMiddleware);
        $router->any('/test/{number}', function (ResponseInterface $response) use ($parent) {
            $stream = $parent->getMockForAbstractClass(StreamInterface::class);
            $stream->method('__toString')->willReturn('route');
            return $response->withBody($stream);
        });
        $result = $router->handle($request);
    
        $this->assertEquals('before-middleware/routed-middleware/route/after-middleware', (string)$result->getBody());
    }
    
    /**
     * @expectedException Apine\DistRoute\MiddlewareQueueException
     * @expectedExceptionMessage Cannot add a middleware once the queue is locked
     */
    public function testHandleCannotAddMiddlewareWhileQueueLocked(): void
    {
        $parent = $this;
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        $request = $this->mockRequest('GET');
        $router = new Router($container);
        
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function ($request, $handler) use ($parent) {
            $middleware = $parent->getMockForAbstractClass(MiddlewareInterface::class);
            $handler->use($middleware);
            return $handler->handle($request);
        });
        
        $router->use($middleware);
        $response = $router->handle($request);
    }
    
    /**
     * @expectedException Apine\DistRoute\MiddlewareQueueException
     * @expectedExceptionMessage Cannot add a route once the queue is locked
     */
    public function testHandleCannotAddRouteWhileQueueLocked(): void
    {
        $parent = $this;
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        $request = $this->mockRequest('GET');
        $router = new Router($container);
        
        $middleware = $this->getMockForAbstractClass(MiddlewareInterface::class);
        $middleware->method('process')->willReturnCallback(function ($request, $handler) {
            $handler->any('*', function () {});
            return $handler->handle($request);
        });
        
        $router->use($middleware);
        $response = $router->handle($request);
    }
    
    /**
     * @expectedException \Apine\DistRoute\RouteNotFoundException
     * @expectedExceptionMessageRegExp /Route for request (.+?) not found/
     */
    public function testHandleNoMatchFound(): void
    {
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/1567/other');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
    
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $router->get('/test/{number}', function(){});
        
        $response = $router->handle($request);
    }
    
    /**
     * @expectedException \RuntimeException
     */
    public function testHandleErrorExecution(): void
    {
        $request = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getMethod', 'getUri'])
            ->getMockForAbstractClass();
    
        $uri = $this->getMockBuilder(UriInterface::class)
            ->setMethods(['getPath'])
            ->getMockForAbstractClass();
    
        $uri->method('getPath')->willReturn('/test/1567');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');
    
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $router = new Router($container);
        $router->get('/test/{number}', function(){});
    
        $response = $router->handle($request);
    }
    
    private function mockRequest(string $verb): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn($verb);
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
    
        return $mockRequest;
    }
}
