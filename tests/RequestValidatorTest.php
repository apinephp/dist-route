<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

use Apine\DistRoute\RequestValidator;
use Apine\DistRoute\RegexBuilderInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class RequestValidatorTest extends TestCase
{
    
    public function testMatch(): void
    {
        $validator = new RequestValidator(
            $this->mockBuilder(),
            ['GET']
        );
        $result = $validator->match($this->mockRequest());
        
        $this->assertTrue($result);
    }
    
    public function testMatchWhenRequestPathNotMatchReturnFalse(): void
    {
        $validator = new RequestValidator(
            $this->mockBuilder(),
            ['GET']
        );
    
        $result = $validator->match($this->mockRequestTwo());
    
        $this->assertFalse($result);
    }
    
    public function testMatchWhenMethodNotAllowedReturnFalse(): void
    {
        $validator = new RequestValidator(
            $this->mockBuilder(),
            ['POST']
        );
        $result = $validator->match($this->mockRequest());
    
        $this->assertFalse($result);
    }
    
    public function testMatchWhenAllowedMethodsEmptyAlwaysReturnTrue(): void
    {
        $request = $this->mockRequest();
        
        $validator = new RequestValidator(
            $this->mockBuilder()
        );
        $result = $validator->match($request);
        $this->assertTrue($result);
    
        $request->method('getMethod')->willReturn('CUSTOM');
        $result2 = $validator->match($request);
        $this->assertTrue($result2);
    }
    
    private function mockBuilder(): RegexBuilderInterface
    {
        /** @var RegexBuilderInterface | MockObject $mockBuilder */
        $mockBuilder = $this->getMockBuilder(RegexBuilderInterface::class)
            ->setMethods(['build'])
            ->getMockForAbstractClass();
        $mockBuilder->method('build')->willReturn('/^\/test\/([0-9]+)(\/([^\/]+?))?$/');
        
        return $mockBuilder;
    }
    
    private function mockRequest(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('GET');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/test/123456789');
            return $mockUri;
        });
        
        return $mockRequest;
    }
    
    private function mockRequestTwo(): ServerRequestInterface
    {
        /** @var ServerRequestInterface | MockObject $mockRequest */
        $mockRequest = $this->getMockBuilder(ServerRequestInterface::class)
            ->setMethods(['getUri', 'getMethod'])
            ->getMockForAbstractClass();
        $mockRequest->method('getMethod')->willReturn('GET');
        $mockRequest->method('getUri')->willReturnCallback(function() {
            /** @var UriInterface | MockObject $mockUri */
            $mockUri = $this->getMockBuilder(UriInterface::class)
                ->setMethods(['getPath'])
                ->getMockForAbstractClass();
            $mockUri->method('getPath')->willReturn('/123456789');
            return $mockUri;
        });
    
        return $mockRequest;
    }
}
