<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface QueryValidatorInterface
 *
 * @package Apine\DistRoute
 */
interface RequestValidatorInterface
{
    public function match(ServerRequestInterface $request): bool;
}