<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Apine\Resolver\DependencyResolver;
use Closure;
use Exception;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use function sprintf;

/**
 * A lightweight fully PSR-7 compatible regex based request router
 * with dependency injection using a DI container
 *
 * @package Apine\DistRoute
 */
final class Router implements RouterInterface
{
    /**
     * List or available routes
     *
     * @var MiddlewareInterface[]
     */
    private $middlewares = [];
    
    /**
     * @var bool
     */
    private $locked = false;
    
    /**
     * DI Container
     *
     * @var ContainerInterface|null
     */
    private $container;
    
    /**
     * Base route pattern
     *
     * @var string
     */
    private $basePattern = '';
    
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    /**
     * Return the base pattern used when add new routes
     *
     * @return string
     */
    public function getBasePattern() : string
    {
        return $this->basePattern;
    }
    
    /**
     * Set the base pattern added to the pattern of a new route
     * Changing this value will only affect the routes to be added
     * after changing it.
     *
     * @param string $pattern
     */
    public function setBasePattern(string $pattern): void
    {
        $this->basePattern = $pattern;
    }
    
    /**
     * Dispatch a request
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     *
     * @throws Exception
     * @throws RouteNotFoundException If no matching route found
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
    
        $dispatcher = clone $this;
        $dispatcher->locked = true;
    
        if (count($dispatcher->middlewares) === 0) {
            throw new RouteNotFoundException(sprintf('Route for request %s not found', $request->getUri()->getPath()));
        }
    
        $middleware = array_shift($dispatcher->middlewares);
    
        return $middleware->process($request, $dispatcher);
    }
    
    /**
     * Add a middleware to be executed during the routing process
     *
     * @param MiddlewareInterface $middleware
     *
     * @return MiddlewareInterface
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function use(MiddlewareInterface $middleware): MiddlewareInterface
    {
        if ($this->locked) {
            throw new MiddlewareQueueException('Cannot add a middleware once the queue is locked');
        }
        
        $this->middlewares[] = $middleware;
        return $middleware;
    }
    
    /**
     * Map a new route to one or multiple request methods
     *
     * @inheritdoc RouteMappingInterface::map()
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function map(array $methods, string $pattern, $callable): RouteInterface
    {
        if ($this->locked) {
            throw new MiddlewareQueueException('Cannot add a route once the queue is locked');
        }
        
        $pattern = $this->basePattern . $pattern;
        $route = new Route(
            new DependencyResolver($this->container),
            $methods,
            $pattern,
            $callable
        );
        $this->middlewares[] = $route;
        
        return $route;
    }
    
    /**
     * Add multiple routes under a prefix
     *
     * @inheritdoc RouteMappingInterface::group()
     */
    public function group(string $pattern, Closure $closure): void
    {
        $currentBase = $this->basePattern;
        $this->basePattern .= $pattern;
        
        $closure($this);
        
        $this->basePattern = $currentBase;
    }
    
    /**
     * Add a route responding to the GET method
     *
     * @inheritdoc RouteMappingInterface::get()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function get(string $pattern, $callable): RouteInterface
    {
        return $this->map(['GET'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to the POST method
     *
     * @inheritdoc RouteMappingInterface::post()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function post(string $pattern, $callable): RouteInterface
    {
        return $this->map(['POST'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to the PUT method
     *
     * @inheritdoc RouteMappingInterface::put()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function put(string $pattern, $callable): RouteInterface
    {
        return $this->map(['PUT'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to the DELETE method
     *
     * @inheritdoc RouteMappingInterface::delete()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function delete(string $pattern, $callable): RouteInterface
    {
        return $this->map(['DELETE'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to the OPTION method
     *
     * @inheritdoc RouteMappingInterface::options()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function options(string $pattern, $callable): RouteInterface
    {
        return $this->map(['OPTIONS'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to the HEAD method
     *
     * @inheritdoc RouteMappingInterface::head()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function head(string $pattern, $callable): RouteInterface
    {
        return $this->map(['HEAD'], $pattern, $callable);
    }
    
    /**
     * @inheritdoc RouteMappingInterface::trace()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function trace(string $pattern, $callable): RouteInterface
    {
        return $this->map(['TRACE'], $pattern, $callable);
    }
    
    /**
     * Add a route responding to any request method
     *
     * @inheritdoc RouteMappingInterface::any()
     * @see RouterInterface::map()
     *
     * @return Route
     * @throws \Apine\DistRoute\MiddlewareQueueException
     */
    public function any(string $pattern, $callable): RouteInterface
    {
        return $this->map([], $pattern, $callable);
    }
}