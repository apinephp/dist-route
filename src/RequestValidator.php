<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Class QueryValidator
 *
 * @package Apine\DistRoute
 */
class RequestValidator implements RequestValidatorInterface
{
    /**
     * @var RegexBuilderInterface
     */
    private $builder;
    
    /**
     * @var string[]
     */
    private $allowedMethods;
    
    public function __construct(
        RegexBuilderInterface $builder,
        array $allowedMethods = []
    ) {
        $this->builder = $builder;
        $this->allowedMethods = $allowedMethods;
    }
    
    public function match(ServerRequestInterface $request): bool
    {
        $allowedMethods = $this->allowedMethods;
        $requestMethod = $request->getMethod();
        $requestPath = $request->getUri()->getPath();
        $regex = $this->builder->build();
    
        if (count($allowedMethods) === 0) {
            $allowedMethods[] = $requestMethod;
        }
    
        return (
            in_array($requestMethod, $allowedMethods, true) &&
            (preg_match($regex, $requestPath) === 1)
        );
    }
}