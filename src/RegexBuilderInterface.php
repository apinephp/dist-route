<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

/**
 * Interface RegexBuilderInterface
 *
 * @package Apine\DistRoute
 */
interface RegexBuilderInterface
{
    public function build(): string;
}