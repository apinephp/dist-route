<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ArgumentExtractor
 *
 * @package Apine\DistRoute
 */
class ArgumentExtractor implements ArgumentExtractorInterface
{
    /**
     * @var ArgumentParserInterface
     */
    private $parser;
    
    /**
     * @var RegexBuilderInterface
     */
    private $builder;
    
    public function __construct(
        ArgumentParserInterface $parser,
        RegexBuilderInterface $builder
    ) {
        $this->parser = $parser;
        $this->builder = $builder;
    }
    
    public function extract(ServerRequestInterface $request): array
    {
        $arguments = $this->parser->parse();
        $queryString = $request->getUri()->getPath();
        
        preg_match_all($this->builder->build(), $queryString, $regexValues, PREG_UNMATCHED_AS_NULL|PREG_SET_ORDER);
        $queryArguments = [];
    
        if (!empty($regexValues)) {
            $regexValues = $regexValues[0];
            array_shift($regexValues);
            
            foreach ($arguments as $argument) {
                foreach ($regexValues as $index => $value) {
                    if (preg_match('#^' . $argument->pattern . '$#', $value)) {
                        $queryArguments[$argument->name] = $value;
                        unset($regexValues[$index]);
                        break;
                    }
                }
            }
        }
    
        return $queryArguments;
    }
}