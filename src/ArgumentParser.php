<?php
/**
 * APIne DistRoute
 *
 * @link      https://gitlab.com/apinephp/dist-route
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/dist-route/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\DistRoute;

/**
 * Class ArgumentParser
 *
 * @package Apine\DistRoute
 */
class ArgumentParser implements ArgumentParserInterface
{
    /**
     * @var string
     */
    private $pattern;
    
    public function __construct(string $pattern)
    {
        $this->pattern = $pattern;
    }
    
    public function parse(): array
    {
        preg_match_all('/\{(\??)(\w+?)(:(\(.+?\)))?\}/', $this->pattern, $matches, PREG_SET_ORDER);
    
        return array_map(function (array $match): Argument {
            return new Argument(
                $match[2],
                $match[4] ?? '([^\/]+?)',
                $match[1] === '?'
            );
        }, $matches);
    }
}